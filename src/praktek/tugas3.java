package praktek;

import java.util.Scanner;

public class tugas3 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String huruf;
        System.out.printf("Input Kata   : ");
        huruf = scan.next();

        StringBuffer huruf1 = new StringBuffer(huruf);
        System.out.println("Reverse Kata : " + huruf1.reverse());
        String data = huruf1.toString();

        if (huruf.equals(data)) {
            System.out.println("Status       : Palindrome ");
        } else {
            System.out.println("Status       : Bukan Palindrome ");
        }
    }
}
