package praktek;
import java.util.Scanner;
public class tugas1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Input  : ");
        String input = sc.next();
        String result = input.replace("a", "4").replaceAll("A", "4").
                replaceAll("i", "1").replaceAll("I", "1").
                replaceAll("e", "3").replaceAll("E", "3").
                replaceAll("o", "0").replaceAll("O", "0").
                replaceAll("u", "11").replaceAll("U", "11");
                
        System.out.println("Output : " + result);
    }
}
